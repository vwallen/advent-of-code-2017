package main

import (
	"testing"
	"reflect"
)

func TestDistributeBlocks(t *testing.T) {
	banks := []int{2, 4, 1, 2}
	DistributeBlocks(banks)
	if !reflect.DeepEqual(banks, []int{3, 1, 2, 3}) {
		t.Fatal("Distribution failed", banks)
	}
}

func TestPart1(t *testing.T) {
	if result, _ := Part1([]int{0, 2, 7, 0}); result != 5 {
		t.Fatal("Part 1 failed", result)
	}
}

func TestPart2(t *testing.T) {
	if result, _ := Part2([]int{0, 2, 7, 0}); result != 4 {
		t.Fatal("Part 2 failed", result)
	}
}