package main

import (
	"os"
	"bufio"
	"fmt"
	"strconv"
)

func prepareInput() ([]int, error) {

	file, err := os.Open("day5/input.txt")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	input := make([]int, 0)
	for scanner.Scan() {
		line := scanner.Text()
		value, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}
		input = append(input, value)
	}

	return input, nil

}

func Part1(input []int) int {

	steps := 0

	// Make a copy to avoid destructive changes
	maze := make([]int, len(input))
	copy(maze, input)

	for i, bounds := 0, len(maze);
		i > -1 && i < bounds;
	    steps++ {

		i, maze[i] = i+maze[i], maze[i] + 1

	}

	return steps
}

func Part2(input []int) int {

	steps := 0

	// Make a copy to avoid destructive changes
	maze := make([]int, len(input))
	copy(maze, input)

	for i, bounds := 0, len(maze);
		i > -1 && i < bounds;
	    steps++ {

		if maze[i] > 2 {
			i, maze[i] = i + maze[i], maze[i] - 1
		} else {
			i, maze[i] = i + maze[i], maze[i] + 1
		}
	}

	return steps
}

func main() {

	input, err := prepareInput()
	if err != nil {
		fmt.Println("Unable to prepare input")
		fmt.Println(err)
		return
	}

	part1Result := Part1(input)
	part2Result := Part2(input)

	fmt.Println("Result for Part 1:", part1Result)
	fmt.Println("Result for Part 2:", part2Result)
}
