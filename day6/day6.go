package main

import (
	"fmt"
)

var MEMORY_BANKS = []int{5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6}

func MaxInt(list []int) int {
	max := 0
	index := -1
	for i, n := range list {
		if n > max {
			index = i
			max = n
		}
	}
	return index
}

func DistributeBlocks(banks []int) {
	indexMax := MaxInt(banks)
	blocks := banks[indexMax]
	numBanks := len(banks)

	banks[indexMax] = 0
	for i := indexMax; blocks > 0; blocks-- {
		i = (i + 1) % numBanks
		banks[i]++
	}
}

func CreateKey(input []int) string {
	return fmt.Sprint(input)
}

func Part1(input []int) (int, []int) {

	banks := make([]int, len(input))
	copy(banks, input)

	history := make(map[string]int)
	result := 0

	for {
		key := CreateKey(banks)
		if history[key] > 0 {
			break
		}
		history[key]++
		DistributeBlocks(banks)
		result++
	}

	return result, banks
}

func Part2(input []int) (int, []int) {

	_, banks := Part1(input)
	return Part1(banks)
}

func main() {

	part1Result, _ := Part1(MEMORY_BANKS)
	part2Result, _ := Part2(MEMORY_BANKS)

	fmt.Println("Result for Part 1:", part1Result)
	fmt.Println("Result for Part 2:", part2Result)

}
