package main

import (
	"testing"
	"fmt"
)

func TestParsingAndScoring(t *testing.T) {

	var node *Node

	node = parseStream("<>")
	if node.serialize() != "<>" {
		fmt.Println(node.serialize())
		t.Fatal()
	}

	node = parseStream("<random characters>")
	if node.serialize() != "<random characters>" {
		fmt.Println(node.serialize())
		t.Fatal()
	}

	node = parseStream("<<<<>")
	if node.serialize() != "<<<<>" {
		fmt.Println(node.serialize())
		t.Fatal()
	}

	node = parseStream("<{!>}>")
	if node.serialize() != "<{!>}>" {
		fmt.Println(node.serialize())
		t.Fatal()
	}

	node = parseStream("<!!>")
	if node.serialize() != "<!!>" {
		fmt.Println(node.serialize())
		t.Fatal()
	}

	node = parseStream("<!!!>>")
	if node.serialize() != "<!!!>>" {
		fmt.Println(node.serialize())
		t.Fatal()
	}

	node = parseStream("<{o\"i!a,<{i<a>")
	if node.serialize() != "<{o\"i!a,<{i<a>" {
		fmt.Println(node.serialize())
		t.Fatal()
	}

	node = parseStream("{}")
	if node.score() != 1 {
		t.Fatal()
	}

	node = parseStream("{{},{}}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 5 {
		t.Fatal()
	}

	node = parseStream("{{{}}}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 6 {
		t.Fatal()
	}

	node = parseStream("{{{},{},{{}}}}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 16 {
		t.Fatal()
	}

	node = parseStream("{<a>,<a>,<a>,<a>}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 1 {
		t.Fatal()
	}

	node = parseStream("{{<ab>},{<ab>},{<ab>},{<ab>}}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 9 {
		t.Fatal()
	}

	node = parseStream("{{<!!>},{<!!>},{<!!>},{<!!>}}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 9 {
		t.Fatal()
	}

	node = parseStream("{{<a!>},{<a!>},{<a!>},{<ab>}}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 3 {
		t.Fatal()
	}

	node = parseStream("{{<!>},{<!>},{<!>},{<a>}}")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 3 {
		t.Fatal()
	}

	node = parseStream("<{o\"i!a,<{i<a>")
	fmt.Println(node.serialize(), node.score())
	if node.score() != 0 {
		t.Fatal()
	}

}

func TestGarbageRemoval(t *testing.T) {

	var node *Node

	node = parseStream("<>") // 0
	if len(node.cleaned) != 0 {
		fmt.Println(node.cleaned)
		t.Fatal()
	}

	node = parseStream("<random characters>") // 17
	if len(node.cleaned) != 17 {
		fmt.Println(node.cleaned)
		t.Fatal()
	}

	node = parseStream("<<<<>") // 3
	if len(node.cleaned) != 3 {
		fmt.Println(node.cleaned)
		t.Fatal()
	}

	node = parseStream("<{!>}>") // 2
	if len(node.cleaned) != 2 {
		fmt.Println(node.cleaned)
		t.Fatal()
	}

	node = parseStream("<!!>") // 0
	if len(node.cleaned) != 0 {
		fmt.Println(node.cleaned)
		t.Fatal()
	}

	node = parseStream("<!!!>>") // 0
	if len(node.cleaned) != 0 {
		fmt.Println(node.cleaned)
		t.Fatal()
	}

	node = parseStream("<{o\"i!a,<{i<a>") // 10
	if len(node.cleaned) != 10 {
		fmt.Println(node.cleaned)
		t.Fatal()
	}

}
