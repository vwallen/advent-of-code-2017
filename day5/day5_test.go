package main

import "testing"

func TestPart1(t *testing.T) {

	if result := Part1([]int{0, 3, 0, 1, -3}); result != 5 {
		t.Fatal("Part 1, This many steps:", result)
	}

}

func TestPart2(t *testing.T) {

	if result := Part2([]int{0, 3, 0, 1, -3}); result != 10 {
		t.Fatal("Part 2, This many steps:", result)
	}

}
