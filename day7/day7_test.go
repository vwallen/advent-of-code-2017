package main

import (
	"testing"
	"fmt"
)

func preparePrograms() map[string]*Program {

	input := make(map[string]*Program)

	lines := []string{
		"pbga (66)",
		"xhth (57)",
		"ebii (61)",
		"havc (66)",
		"ktlj (57)",
		"fwft (72) -> ktlj, cntj, xhth",
		"qoyq (66)",
		"padx (45) -> pbga, havc, qoyq",
		"tknk (41) -> ugml, padx, fwft",
		"jptl (61)",
		"ugml (68) -> gyxo, ebii, jptl",
		"gyxo (61)",
		"cntj (57)",
	}

	for _, line := range lines {
		name, weight, children, _ := parseInput(line)
		prepareProgram(input, name, weight, children)
	}

	return input
}

func TestPart1(t *testing.T) {

	input := preparePrograms()

	if result := Part1(input); result != "tknk" {
		t.Fatal("That's not what I expected for Part 1:", result)
	}

}

func TestPart2(t *testing.T) {

	input := preparePrograms()

	root := input[Part1(input)]

	balanced, program := root.checkBalance()
	if balanced {
		t.Fatal("Tower should not be balanced")
	}

	fmt.Println("Unbalanced program:", program)
	for _, child := range program.children {
		fmt.Println(child.fullWeight())
	}

	problem, diff := program.findImbalance()
	fmt.Println(problem, diff)

	if result := Part2(input); result != 60 {
		t.Fatal("That's not what I expected for Part 2:", result)
	}

}