package main

import (
	"testing"
)

func TestPart1(t *testing.T) {
	result := Part1([][]int{
		{5, 1, 9, 5},
		{7, 5, 3},
		{2, 4, 6, 8},
	})
	if result != 18 {
		t.Fatal()
	}
}

func TestPart2(t *testing.T) {
	result := Part2([][]int{
		{5, 9, 2, 8},
		{9, 4, 7, 3},
		{3, 8, 6, 5},
	})
	if result != 9 {
		t.Fatal()
	}
}