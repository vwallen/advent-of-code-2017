package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
)


func prepareInput() ([]int, error) {

	// TODO - There's got to be a better way

	input, err := ioutil.ReadFile("day1/input.txt")
	if err != nil {
		return nil, err
	}

	inputLen := len(input)
	inputList := make([]int, inputLen)

	for i := 0; i < inputLen; i++ {
		inputList[i], err = strconv.Atoi(string(input[i]))
		if err != nil {
			return nil, err
		}
	}

	return inputList, nil
}

func Part1(input []int) int {
	result := 0
	size := len(input)
	for i, val := range input {
		if val == input[(i + 1) % size] {
			result += val
		}
	}
	return result
}

func Part2(input []int) int {
	result := 0
	size := len(input)
	offset := size/2
	for i, val := range input {
		if val == input[(i + offset) % size] {
			result += val
		}
	}
	return result
}

func main() {

	input, err := prepareInput()
	if err != nil {
		fmt.Println("Unable to prepare input")
		fmt.Println(err)
		return
	}

	part1Result := Part1(input)
	part2Result := Part2(input)

	fmt.Println("Result of part 1:", part1Result)
	fmt.Println("Result of part 2:", part2Result)
}
