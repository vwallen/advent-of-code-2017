package main

import (
	"os"
	"bufio"
	"fmt"
	"strings"
)


type Node struct {
	garbage bool
	content string
	cleaned string
	parent *Node
	children []*Node
}

func (self *Node) serialize() string {
	if self.garbage {
		return "<" + self.content + ">"
	}
	children := make([]string, len(self.children))
	for i, child := range self.children {
		children[i] = child.serialize()
	}
	return "{" + strings.Join(children, ",") + "}"
}

func (self *Node) addChild(child *Node) *Node {
	self.children = append(self.children, child)
	child.parent = self
	return child
}

func (self *Node) depth() int {
	if self.parent == nil {
		return 1
	}
	return self.parent.depth() + 1
}

func (self *Node) score() int {

	if self.garbage {
		return 0
	}
	score := self.depth()
	if self.children != nil {
		for _, child := range self.children {
			score += child.score()
		}
	}
	return score
}

func (self *Node) scoreGarbage() int {
	if self.garbage {
		return len(self.cleaned)
	}
	score := 0
	if self.children != nil {
		for _, child := range self.children {
			score += child.scoreGarbage()
		}
	}
	return score
}

//================================================

func parseStream(input string) *Node {

	var node *Node
	nodes := make([]*Node, 0)

	chars := []rune(input)
	garbage := false
	for i := 0; i < len(input); i++ {
		char := chars[i]

		if garbage {
			switch char {
			case '!':
				// skip next character
				i += 1
				node.content += string(char) + string(chars[i])

			case '>':
				// close garbage node
				garbage = false
				nodes = nodes[:len(nodes) - 1]
				node = nodes[len(nodes) - 1]

			default:
				// append content
				node.content += string(char)
				node.cleaned += string(char)
			}
		} else {
			switch char {
			case '{':
				// open gropu node
				if node == nil {
					node = &Node{}
					nodes = append(nodes, node)
				} else {
					node = node.addChild(&Node{})
				}
				nodes = append(nodes, node)

			case '}':
				// close group node
				nodes = nodes[:len(nodes) - 1]
				node = nodes[len(nodes) - 1]

			case '<':
				// open garbage node
				garbage = true
				if node == nil {
					node = &Node{}
					node.garbage = true
					nodes = append(nodes, node)
				} else {
					node = node.addChild(&Node{garbage: true})
				}
				nodes = append(nodes, node)
			}
		}

	}
	return node
}

func prepareInput() (*Node, error) {

	file, err := os.Open("day9/input.txt")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var input *Node
	var line string
	for scanner.Scan() {
		line = scanner.Text()
		input = parseStream(line)
		break
	}

	if input.serialize() != line {
		panic("No round trip!")
	}

	return input, err
}

func Part1(input *Node) int {
	return input.score()
}

func Part2(input *Node) int {
	return input.scoreGarbage()
}

func main() {

	input, err := prepareInput()
	if err != nil {
		fmt.Println("Unable to prepare input")
		fmt.Println(err)
		return
	}

	part1Result := Part1(input)
	part2Result := Part2(input)

	fmt.Println("Result for Part 1:", part1Result)
	fmt.Println("Result for Part 2:", part2Result)
}
