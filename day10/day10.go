package main

import (
	"fmt"
)

var INPUT = []int{76,1,88,148,166,217,130,0,128,254,16,2,130,71,255,229}

func Part1(input []int) int {
	return 0
}

func Part2(input []int) int {
	return 0
}

func main() {

	part1Result := Part1(INPUT)
	part2Result := Part2(INPUT)

	fmt.Println("Result for Part 1:", part1Result)
	fmt.Println("Result for Part 2:", part2Result)
}
