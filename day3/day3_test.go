package main

import "testing"

func TestFindGridPosition(t *testing.T) {

	var x int
	var y int

	x, y = FindGridPosition(1)
	if x != 0 || y != 0 {
		t.Fatal()
	}

	x, y = FindGridPosition(4)
	if x != 0 || y != -1 {
		t.Fatal()
	}

	x, y = FindGridPosition(9)
	if x != 1 || y != 1 {
		t.Fatal()
	}

	x, y = FindGridPosition(16)
	if x != -1 || y != -2 {
		t.Fatal()
	}

	x, y = FindGridPosition(13)
	if x != 2 || y != -2 {
		t.Fatal()
	}

	x, y = FindGridPosition(19)
	if x != -2 || y != 0 {
		t.Fatal()
	}

	x, y = FindGridPosition(23)
	if x != 0 || y != 2 {
		t.Fatal()
	}

}

func TestPart1(t *testing.T) {
	if Part1(1) != 0 {
		t.Fatal()
	}
	if Part1(12) != 3 {
		t.Fatal()
	}
	if Part1(23) != 2 {
		t.Fatal()
	}
	if Part1(1024) != 31 {
		t.Fatal()
	}
}

func TestPart2(t *testing.T) {
	// TODO - Refactor for testability
}
