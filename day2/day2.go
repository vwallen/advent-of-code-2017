package main

import (
	"fmt"
	"os"
	"bufio"
	"strings"
	"strconv"
	"sort"
)

func prepareInput() ([][]int, error) {

	file, err := os.Open("day2/input.txt")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var rows int = 0
	var cols int = 0
	cells := make([]int, 0)

	for scanner.Scan() {
		line := scanner.Text()
		values := strings.Split(line, "\t")

		rows += 1
		cols = len(values)

		for _, value := range values {
			intvalue, err := strconv.Atoi(value)
			if err != nil {
				return nil, err
			}
			cells = append(cells, intvalue)
		}
	}

	input := make([][]int, rows)

	for i, cell := range cells {
		input[i / cols] = append(input[i / cols], cell)
	}

	return input, nil
}

func MaxInt(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func MinInt(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func Part1(input [][]int) int {

	result := 0
	for _, row := range input {
		// Making a copy of the row so it can
		// be sorted non-destructively
		values := make([]int, len(row))
		copy(values, row)
		sort.Ints(values)
		result += values[len(values)-1] - values[0]
	}

	return result
}

func Part2(input [][]int) int {
	result := 0
	var a int;
	var b int;
	for _, row := range input {
		for i, cell := range row {
			for j := i + 1; j < len(row); j++ {
				a = MinInt(cell, row[j])
				b = MaxInt(cell, row[j])
				if b % a == 0 {
					result += b/a
				}
			}
		}
	}

	return result
}

func main() {

	input, err := prepareInput()
	if err != nil {
		fmt.Println("Unable to prepare input")
		fmt.Println(err)
		return
	}

	part1Result := Part1(input)
	part2Result := Part2(input)

	fmt.Println("Result for Part 1:", part1Result)
	fmt.Println("Result for Part 2:", part2Result)
}
