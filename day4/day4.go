package main

import (
	"strings"
	"os"
	"bufio"
	"fmt"
	"sort"
)


func prepareInput() ([]string, error) {

	file, err := os.Open("day4/input.txt")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	input := make([]string, 0)
	for scanner.Scan() {
		line := scanner.Text()
		input = append(input, line)
	}

	return input, nil
}

func SortString(instring string) string {
	outstring := strings.Split(instring, "")
	sort.Strings(outstring)
	return strings.Join(outstring, "")
}

func ValidatePassphrase(passphrase string) bool {

	words := strings.Split(passphrase, " ")
	wordMap := make(map[string]bool)

	for _, word := range words {
		wordMap[word] = true
	}

	return len(words) == len(wordMap)
}

func ValidateAnagramPassphrase(passphrase string) bool {

	words := strings.Split(passphrase, " ")
	wordMap := make(map[string]bool)

	for _, word := range words {
		sorted := SortString(word)
		wordMap[sorted] = true
	}

	return len(words) == len(wordMap)
}


func Part1(input []string) int {

	result := 0
	for _, passphrase := range input {
		if ValidatePassphrase(passphrase) {
			result++
		}
	}

	return result
}

func Part2(input []string) int {

	result := 0
	for _, passphrase := range input {
		if ValidateAnagramPassphrase(passphrase) {
			result++
		}
	}

	return result
}


func main() {

	input, err := prepareInput()
	if err != nil {
		fmt.Println("Unable to prepare input")
		fmt.Println(err)
		return
	}

	part1Result := Part1(input)
	part2Result := Part2(input)

	fmt.Println("Result for Part 1:", part1Result)
	fmt.Println("Result for Part 2:", part2Result)
}
