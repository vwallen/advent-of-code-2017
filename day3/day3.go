package main

import (
	"math"
	"fmt"
)

const INPUT_ADDRESS int = 277678

func AbsInt(n int) int {
	if n < 0 {
		return -n
	}
	return n
}

func FindGridPosition(input int) (int, int) {

	// Start counting from the closest square value
	// since we can predictably determine its position
	previousRoot := int(math.Floor(math.Sqrt(float64(input))))
	evenRoot := int(previousRoot) % 2 == 0

	// Default offset is half of the root value
	// for each square, rounded down
	prX := int(math.Floor(float64(previousRoot)/2))
	prY := int(math.Floor(float64(previousRoot)/2))

	distance := input - (previousRoot * previousRoot)

	if distance > 0 {

		prX += 1      // start a new column
		distance -= 1 // and reduce distance remaining

		if distance > previousRoot {
			// the position will wrap around
			prX -= distance - previousRoot
			prY -= previousRoot
		} else {
			// the position will not wrap
			prY -= distance
		}
	}

	// even numbered roots are up and left
	// flip the values to adjust and
	// offset by 1 due to start pos
	if evenRoot {
		prX = -(prX - 1)
		prY = -prY
	}

	return prX, prY
}

func CalculateValueAtPosition(grid [][]int, x int, y int) int {

	// This assumes the grid was initialized with 0
	// values in all positions and no positions
	// have been populated but those previous
	// to the given position

	value := 0

	// There's some slice magic that will
	// make this neater, I'm sure
	value += grid[x - 1][y - 1]
	value += grid[x - 1][y]
	value += grid[x - 1][y + 1]
	value += grid[x][y - 1]
	value += grid[x][y]
	value += grid[x][y + 1]
	value += grid[x + 1][y - 1]
	value += grid[x + 1][y]
	value += grid[x + 1][y + 1]

	return value
}

func Part1(input int) int {
	x, y := FindGridPosition(input)
	return AbsInt(x) + AbsInt(y)
}

func Part2(threshold int) int {

	result := 0

	// Build a reasonably large grid to hold our spiral
	// this will allow enough space to manually build the
	// grid based on offset positioning
	offset := 10
	grid := make([][]int, offset * 2)
	for i, _ := range grid {
		grid[i] = make([]int, offset * 2)
	}

	grid[offset][offset] = 1

	for i := 1; i < threshold; i++ {
		x, y := FindGridPosition(i + 1)
		result = CalculateValueAtPosition(grid, x + offset, y + offset)
		if result > threshold {
			break
		}
		grid[x + offset][y + offset] = result
	}

	return result
}

func main() {

	part1Result := Part1(INPUT_ADDRESS)
	part2Result := Part2(INPUT_ADDRESS)

	fmt.Println("Result from Part 1:", part1Result)
	fmt.Println("Result from Part 2:", part2Result)
}
