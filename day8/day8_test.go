package main

import (
	"testing"
)

func prepareTestInput() []Instruction {

	input := []string {
		"b inc 5 if a > 1",
		"a inc 1 if b < 5",
		"c dec -10 if a >= 1",
		"c inc -20 if c == 10",
	}

	output := make([]Instruction, 0)
	for _, i := range input {
		output = append(output, ParseInstruction(i))
	}

	return output
}

func TestPart1(t *testing.T) {

	input := prepareTestInput()
	result := Part1(input)
	if result != 1 {
		t.Fatal("Wrong registers value")
	}
}

func TestPart2(t *testing.T) {

	input := prepareTestInput()
	result := Part2(input)
	if result != 10 {
		t.Fatal("Wrong registers value")
	}
}
