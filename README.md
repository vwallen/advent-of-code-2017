# Advent of Code 2017

Challenges are from the [Advent of Code](http://adventofcode.com/2017/)

Trying again this year, hopefully this is the year I actually finish.

This year I will be using the advent ot learn Go. Suggestions are welcome!

**SPOILER WARNING**: *The exercise text is included in the README for each day, so don't look ahead!* 