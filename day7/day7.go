package main

import (
	"os"
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

type Program struct {
	name string
	weight int
	parent *Program
	children []*Program
}

func (self *Program) fullWeight() int {
	weight := self.weight
	if self.children != nil {
		for _, child := range self.children {
			weight += child.fullWeight()
		}
	}
	return weight
}

func (self *Program) checkBalance() (bool, *Program) {

	if self.children != nil {
		weight := self.children[0].fullWeight()
		for _, child := range self.children {
			if balanced, program := child.checkBalance(); !balanced {
				return false, program
			}
			if child.fullWeight() != weight {
				return false, self
			}
		}
	}

	return true, nil
}

func (self *Program) findImbalance() (*Program, int) {

	if self.children == nil {
		return nil, 0
	}

	outlier := 0
	for i, child := range self.children {
		if self.children[0].fullWeight() != child.fullWeight() {
			if outlier != 0 {
				return self.children[0], self.children[1].fullWeight() - self.children[0].fullWeight()
			}
			outlier = i
		}
	}

	return self.children[outlier], self.children[0].fullWeight() - self.children[outlier].fullWeight()
}

func parseInput(line string) (string, int, []string, error) {

	p1 := strings.Split(line, " -> ")
	p2 := strings.Split(p1[0], " ")

	name := p2[0]
	weight, _ := strconv.Atoi(string([]rune(p2[1])[1:len(p2[1]) - 1]))

	var children []string
	if len(p1) > 1 {
		children = strings.Split(p1[1], ", ")
	} else {
		children = []string{}
	}

	return name, weight, children, nil
}

func prepareProgram(programs map[string]*Program, name string, weight int, children []string) {

	program, ok := programs[name]
	if !ok {
		program = &Program{
			name: name,
		}
		programs[name] = program
	}
	program.weight = weight

	for _, childname := range children {
		child, ok := programs[childname]
		if !ok {
			child = &Program{
				name: childname,
			}
			programs[childname] = child
		}
		child.parent = program
		program.children = append(program.children, child)
	}
}


func prepareInput() (map[string]*Program, error) {

	file, err := os.Open("day7/input.txt")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	input := make(map[string]*Program)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		name, weight, children, err := parseInput(line)
		if err != nil {
			return nil, err
		}
		prepareProgram(input, name, weight, children)
	}

	return input, nil

}

func FindRoot(input map[string]*Program) *Program {

	for _, program := range input {
		if program.parent == nil {
			return program
		}
	}
	return nil
}


func Part1(input map[string]*Program) string {
	program := FindRoot(input)
	if program != nil {
		return  program.name
	}
	return ""
}


func Part2(input map[string]*Program) int {

	result := 0

	root := FindRoot(input)
	balanced, program := root.checkBalance()
	if !balanced {
		problem, diff := program.findImbalance()
		result = problem.weight + diff
	}

	return result
}

func main() {

	input, err := prepareInput()
	if err != nil {
		fmt.Println("Unable to prepare input")
		fmt.Println(err)
		return
	}

	part1Result := Part1(input)
	part2Result := Part2(input)

	fmt.Println("Result for Part 1:", part1Result)
	fmt.Println("Result for Part 2:", part2Result)
}
