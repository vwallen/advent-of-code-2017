package main

import "testing"

func TestValidatePassphrase(t *testing.T) {

	if !ValidatePassphrase("aa bb cc dd ee") {
		t.Fatal()
	}
	if ValidatePassphrase("aa bb cc dd aa") {
		t.Fatal()
	}
	if !ValidatePassphrase("aa bb cc dd aaa") {
		t.Fatal()
	}
}

func BenchmarkValidateAnagramPassphrase(b *testing.B) {
	if !ValidateAnagramPassphrase("abcde fghij") {
		b.Fatal()
	}
	if ValidateAnagramPassphrase("abcde xyz ecdab") {
		b.Fatal()
	}
	if !ValidateAnagramPassphrase("a ab abc abd abf abj") {
		b.Fatal()
	}
	if !ValidateAnagramPassphrase("iiii oiii ooii oooi oooo") {
		b.Fatal()
	}
	if ValidateAnagramPassphrase("oiii ioii iioi iiio") {
		b.Fatal()
	}
}
