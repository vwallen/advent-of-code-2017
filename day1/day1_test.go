package main

import (
	"testing"
	"fmt"
)


// This function copied from https://gist.github.com/samalba/6059502
func assertEqual(t *testing.T, a interface{}, b interface{}, message string) {
	if a == b {
		return
	}
	if len(message) == 0 {
		message = fmt.Sprintf("%v != %v", a, b)
	}
	t.Fatal(message)
}

func TestPart1(t *testing.T) {
	assertEqual(t, Part1([]int{1,1,2,2}), 3, "")
	assertEqual(t, Part1([]int{1,1,1,1}), 4, "")
	assertEqual(t, Part1([]int{1,2,3,4}), 0, "")
	assertEqual(t, Part1([]int{9,1,2,1,2,1,2,9}), 9, "")
}

func TestPart2(t *testing.T) {
	assertEqual(t, Part2([]int{1,2,1,2}), 6, "")
	assertEqual(t, Part2([]int{1,2,2,1}), 0, "")
	assertEqual(t, Part2([]int{1,2,3,4,2,5}), 4, "")
	assertEqual(t, Part2([]int{1,2,3,1,2,3}), 12, "")
	assertEqual(t, Part2([]int{1,2,1,3,1,4,1,5}), 4, "")
}